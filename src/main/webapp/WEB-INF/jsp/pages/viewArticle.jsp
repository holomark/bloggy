<%@ page
  info="Display address weather and the eventual result"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  Expects the following atributes:
  - article
 --%>

<c:import url = "../_header.jsp"/>

<section class="section">
  <article class="article media box">
    <div class="media-content">
      <h3 class="title is-5">
        ${ article.title }
      </h3>
      <p>
        Published: <strong>${ article.created }</strong>
      </p>

      <div class="body">
        <c:choose>
          <c:when test="${article.bodyFormat == 'RAW'}">
            <c:out value="${article.body}" />
          </c:when>

          <c:when test="${article.bodyFormat == 'DELTA'}">
            <div class="delta" id="delta-body">
              <c:out value="${article.body}" />
            </div>

            <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
            <script>
              let content = ${article.body}
              let quill = new Quill('#delta-body', {
                readOnly: true
              })

              quill.setContents(content)
            </script>
          </c:when>
        </c:choose>
      </div>
    </div>
  </article>
</section>


<c:import url = "../_footer.jsp"/>
