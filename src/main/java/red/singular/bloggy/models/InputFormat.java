package red.singular.bloggy.models;

public enum InputFormat {
    RAW, DELTA
}
